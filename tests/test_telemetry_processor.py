import unittest
import json
import datetime
from mission_control.telemetry_processor import *
from mission_control.telemetry_file_parser import *
from mission_control.alert_finder import *
from mission_control.models import *
from mission_control.output_formatter import *
from unittest.mock import patch, mock_open
from mission_control.cli import *


class TestTelemetryProcessor(unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()
        parser = TelemetryFileParser(ingest_time_format="%Y%m%d %H:%M:%S.%f")
        alert_finder = AlertFinder(
            interval=datetime.timedelta(0, 0, 0, 0, 5),
            frequency=3,
            filter_fn=default_filter,
        )
        output_formatter = OutputFormatter(
            sort_key="timestamp",
            reverse=True,
            indent=4,
            format_time_fn=format_time,
            format_threshold_fn=format_threshold,
        )
        self.telemetry_processor = TelemetryProcessor(
            parser=parser, alert_finder=alert_finder, outputter=output_formatter
        )

    # test_custom_component tests what happens when an alert is used for a different component than thermostat and battery
    def test_custom_component(self) -> None:
        filter_dict: Dict[SatelliteReadingFilterKey, bool] = {
            SatelliteReadingFilterKey(
                component="ALTMT", threshold=models.Threshold.BEYOND_UPPER
            ): True
        }

        def custom_filter(
            reading: models.SatelliteStatusReading, threshold: models.Threshold
        ) -> bool:
            return (
                filter_dict.get(
                    SatelliteReadingFilterKey(
                        component=reading.component, threshold=threshold
                    )
                )
                or False
            )

        alert_finder = AlertFinder(
            interval=datetime.timedelta(0, 0, 0, 0, 5),
            frequency=3,
            filter_fn=custom_filter,
        )
        self.telemetry_processor.alert_finder = alert_finder

        mock_input = """
20180101 23:59:56.000|1000|101|98|25|20|100.0|ALTMT
20180101 23:59:57.000|1000|101|98|25|20|103.0|ALTMT
20180101 23:59:58.000|1000|101|98|25|20|102.0|ALTMT
20180101 23:59:59.000|1000|101|98|25|20|100.0|ALTMT
20180101 00:00:00.000|1000|101|98|25|20|100.0|ALTMT
20180101 00:00:01.000|1000|101|98|25|20|100.0|ALTMT
20180101 00:00:02.000|1000|101|98|25|20|102.0|ALTMT
20180101 00:00:03.000|1000|101|98|25|20|100.0|ALTMT
20180101 00:00:04.000|1000|101|98|25|20|100.0|ALTMT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            with open("mockfile.dne") as inf:
                expected = json.loads(
                    """
                [
                    {
                        "satelliteId": 1000,
                        "severity": "RED HIGH",
                        "component": "ALTMT",
                        "timestamp": "2018-01-01T23:59:57.000Z"
                    }
                ]
                """
                )

                actual = json.loads(self.telemetry_processor.process(inf))
                self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
