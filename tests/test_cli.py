import unittest
import json
import datetime
from mission_control.telemetry_processor import *
from mission_control.telemetry_file_parser import *
from mission_control.alert_finder import *
from mission_control.models import *
from mission_control.output_formatter import *
from unittest.mock import patch, mock_open
from mission_control.cli import *
import io
from contextlib import redirect_stdout
import os

# used for testing with real files
TEST_DIR = os.path.dirname(os.path.abspath(__file__))


class TestCLI(unittest.TestCase):
    """TestCLI is essentially the integration test case. It primarily simulates opening a file except for a few cases where a real file makes sense"""

    def test_date_creation(self) -> None:
        """test_date_creation tests formatting for displaying datetime"""
        inp = datetime.datetime(2018, 1, 1, 23, 1, 38, 1000)
        expected = "2018-01-01T23:01:38.001Z"
        actual = format_time(inp)
        self.assertEqual(expected, actual)

    # NOTE: this works but I can't add unreadable files to git. I know there are workarounds to this but in the interest of this being a toy project I'm going to skip it
    # test_file_bad_permissions tests what happens when the test file does not give us permission to read it
    # def test_file_bad_permissions(self) -> None:
    #     filename = os.path.join(TEST_DIR, "sat_telemetry_unreadable.dat")
    #     expected = f"{filename} - cannot be read\n"
    #     with io.StringIO() as buf, redirect_stdout(
    #         buf
    #     ):  # redirecting stdout to a buffer instead
    #         main([filename])
    #         actual = buf.getvalue()
    #         self.assertEqual(actual, expected)

    # test_file_actually_a_directory tests what happens when the test file is actually a directory
    def test_file_actually_a_directory(self) -> None:
        expected = f"{TEST_DIR} - is a directory not a file\n"
        with io.StringIO() as buf, redirect_stdout(
            buf
        ):  # redirecting stdout to a buffer instead
            main([TEST_DIR])
            actual = buf.getvalue()
            self.assertEqual(actual, expected)

    # test_file_dne tests what happens when the test file doesn't exist
    def test_file_dne(self) -> None:
        filename = os.path.join(TEST_DIR, "dnefile.dne")
        expected = f"{filename} - does not exist\n"
        with io.StringIO() as buf, redirect_stdout(
            buf
        ):  # redirecting stdout to a buffer instead
            main([filename])
            actual = buf.getvalue()
            self.assertEqual(actual, expected)

    # test_real_file is the basic use case that's outlined in the spec with a real file instead of a mock
    def test_real_file(self) -> None:
        expected = json.loads(
            """
            [
                {
                    "satelliteId": 1000,
                    "severity": "RED HIGH",
                    "component": "TSTAT",
                    "timestamp": "2018-01-01T23:01:38.001Z"
                },
                {
                    "satelliteId": 1000,
                    "severity": "RED LOW",
                    "component": "BATT",
                    "timestamp": "2018-01-01T23:01:09.521Z"
                }
            ]
            """
        )
        with io.StringIO() as buf, redirect_stdout(
            buf
        ):  # redirecting stdout to a buffer instead
            main([os.path.join(TEST_DIR, "sat_telemetry_001.dat")])
            output = buf.getvalue()
            actual = json.loads(output)
            self.assertEqual(actual, expected)

    # test_basic is the basic use case that's outlined in the spec
    def test_basic(self) -> None:
        mock_input = """
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads(
                """
          [
              {
                  "satelliteId": 1000,
                  "severity": "RED HIGH",
                  "component": "TSTAT",
                  "timestamp": "2018-01-01T23:01:38.001Z"
              },
              {
                  "satelliteId": 1000,
                  "severity": "RED LOW",
                  "component": "BATT",
                  "timestamp": "2018-01-01T23:01:09.521Z"
              }
          ]
          """
            )
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_empty_file tests what happens when there are no readings
    def test_empty_file(self) -> None:
        mock_input = """
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads("[]")
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_nonsensical_temp_high_python_int_limit_break tests what happens when the thermostat has nonsensical readings way too high and break python's int limitations
    def test_nonsensical_temp_high_python_int_limit_break(self) -> None:
        mock_input = """
20170101 23:01:05.001|1000|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
20170101 23:01:26.011|1001|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
20180101 23:01:05.001|1001|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
20180101 23:01:26.011|1001|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|92233720368547758071922337203685477580719223372036854775807192233720368547758071|TSTAT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads(
                """
            [
                {
                    "satelliteId": 1001,
                    "severity": "RED HIGH",
                    "component": "TSTAT",
                    "timestamp": "2018-01-01T23:01:05.001Z"
                }
            ]
            """
            )
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_nonsensical_temp_high tests what happens when the thermostat has nonsensical readings way too high
    def test_nonsensical_temp_high(self) -> None:
        mock_input = """
20170101 23:01:05.001|1000|101|98|25|20|9223372036854775807|TSTAT
20170101 23:01:26.011|1001|101|98|25|20|9223372036854775807|TSTAT
20180101 23:01:05.001|1001|101|98|25|20|9223372036854775807|TSTAT
20180101 23:01:26.011|1001|101|98|25|20|9223372036854775807|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|9223372036854775807|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|9223372036854775807|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|9223372036854775807|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|9223372036854775807|TSTAT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads(
                """
            [
                {
                    "satelliteId": 1001,
                    "severity": "RED HIGH",
                    "component": "TSTAT",
                    "timestamp": "2018-01-01T23:01:05.001Z"
                }
            ]
            """
            )
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_nonsensical_battery tests what happens when the battery has nonsensical readings (below 0)
    def test_nonsensical_battery(self) -> None:
        mock_input = """
20170101 23:01:09.521|1000|17|15|9|8|-22.8|BATT
20180101 23:01:09.522|1000|17|15|9|8|-21.6|BATT
20180101 23:02:11.302|1000|17|15|9|8|-25.4|BATT
20180101 23:04:11.531|1000|17|15|9|8|-28.2|BATT
20180101 23:05:07.421|1001|17|15|9|8|-18.0|BATT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads(
                """
            [
                {
                    "satelliteId": 1000,
                    "severity": "RED LOW",
                    "component": "BATT",
                    "timestamp": "2018-01-01T23:01:09.522Z"
                }
            ]
            """
            )
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_no_alerts_for_high_battery tests what happens when the battery is over limit
    def test_no_alerts_for_high_battery(self) -> None:
        mock_input = """
20170101 23:01:09.521|1000|17|15|9|8|22.8|BATT
20180101 23:01:09.522|1000|17|15|9|8|21.6|BATT
20180101 23:02:11.302|1000|17|15|9|8|25.4|BATT
20180101 23:04:11.531|1000|17|15|9|8|28.2|BATT
20180101 23:05:07.421|1001|17|15|9|8|18.0|BATT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads("[]")
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_no_alerts_for_low_temperature tests what happens when the temperature is super low
    def test_no_alerts_for_low_temperature(self) -> None:
        mock_input = """
20170101 23:01:05.001|1001|101|98|25|20|0|TSTAT
20170101 23:01:26.011|1001|101|98|25|20|-20|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|-373|TSTAT
20180101 23:01:05.001|1001|101|98|25|20|-200|TSTAT
20180101 23:01:26.011|1001|101|98|25|20|-200.5|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|-45.5|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|9.2|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|7.2|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|8.8|TSTAT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads("[]")
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_no_alerts tests valid data but no 3-reading alerts within 5 minutes of one another
    def test_no_alerts(self) -> None:
        mock_input = """
20170101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20170101 23:01:09.521|1000|17|15|9|8|8.8|BATT
20170101 23:01:26.011|1001|101|98|25|20|120.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|8.8|BATT
20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|99.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|8.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|100.7|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|8.9|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|8.9|BATT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads("[]")
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_no_alerts tests when battery/thermostat hits the limit but does not exceed it
    def test_no_alerts_at_limit(self) -> None:
        mock_input = """
20170101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20170101 23:01:09.521|1000|17|15|9|8|8|BATT
20170101 23:01:26.011|1001|101|98|25|20|101|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|101|TSTAT
20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
20180101 23:01:09.521|1000|17|15|9|8|8|BATT
20180101 23:01:26.011|1001|101|98|25|20|101|TSTAT
20180101 23:01:38.001|1000|101|98|25|20|101|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT
20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT
20180101 23:02:11.302|1000|17|15|9|8|8|BATT
20180101 23:03:03.008|1000|101|98|25|20|100.7|TSTAT
20180101 23:04:06.017|1001|101|98|25|20|89|TSTAT
20180101 23:04:11.531|1000|17|15|9|8|8|BATT
20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|8|8|BATT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads("[]")
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_overlapping_alerts tests when thermostat exceeds the limit but with overlapping readings over 5 minutes
    def test_overlapping_alerts(self) -> None:
        self.maxDiff = None
        mock_input = """
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:05:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:06:06.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:07:07.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:07:11.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:08:06.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:09:07.009|1000|101|98|25|20|101.2|TSTAT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads(
                """
            [
                {
                    "satelliteId": 1000,
                    "severity": "RED HIGH",
                    "component": "TSTAT",
                    "timestamp": "2018-01-01T23:01:38.001Z"
                }
            ]
            """
            )
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_alerts_on_multiple_satellites tests when thermostat exceeds the limit on different satellites with different thresholds
    def test_alerts_on_multiple_satellites(self) -> None:
        mock_input = """
20180101 23:01:05.001|1001|100|98|25|20|99.9|TSTAT
20180101 23:01:05.017|1001|17|15|9|7|6.2|BATT
20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT
20180101 23:01:26.011|1001|100|98|25|20|100.8|TSTAT
20180101 23:01:27.017|1001|17|15|9|7|6.2|BATT
20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT
20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT
20180101 23:02:09.014|1001|100|98|25|20|100.3|TSTAT
20180101 23:02:10.021|1001|100|98|25|20|100.4|TSTAT
20180101 23:02:11.017|1001|17|15|9|7|6.2|BATT
20180101 23:02:11.270|1001|17|15|9|7|8.2|BATT
20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT
20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT
20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT
20180101 23:04:06.017|1001|100|98|25|20|89.9|TSTAT
20180101 23:04:06.029|1001|17|15|9|7|8.2|BATT
20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT
20180101 23:05:05.021|1001|100|98|25|20|89.9|TSTAT
20180101 23:05:07.421|1001|17|15|9|7|7.9|BATT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads(
                """
              [
                  {
                      "satelliteId": 1000,
                      "severity": "RED HIGH",
                      "component": "TSTAT",
                      "timestamp": "2018-01-01T23:01:38.001Z"
                  },
                  {
                      "satelliteId": 1001,
                      "severity": "RED HIGH",
                      "component": "TSTAT",
                      "timestamp": "2018-01-01T23:01:26.011Z"
                  },
                  {
                      "satelliteId": 1000,
                      "severity": "RED LOW",
                      "component": "BATT",
                      "timestamp": "2018-01-01T23:01:09.521Z"
                  },
                  {
                      "satelliteId": 1001,
                      "severity": "RED LOW",
                      "component": "BATT",
                      "timestamp": "2018-01-01T23:01:05.017Z"
                  }
              ]
              """
            )
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_almost_alert tests what happens when an alerting reading falls just outside the 5 minute window
    def test_almost_alert(self) -> None:
        mock_input = """
20180101 23:01:00.000|1000|101|98|25|20|100.0|TSTAT
20180101 23:02:00.000|1000|101|98|25|20|103.0|TSTAT
20180101 23:03:00.000|1000|101|98|25|20|102.0|TSTAT
20180101 23:04:00.000|1000|101|98|25|20|100.0|TSTAT
20180101 23:05:00.000|1000|101|98|25|20|100.0|TSTAT
20180101 23:06:00.000|1000|101|98|25|20|100.0|TSTAT
20180101 23:07:00.001|1000|101|98|25|20|102.0|TSTAT
20180101 23:08:00.000|1000|101|98|25|20|100.0|TSTAT
20180101 23:09:00.000|1000|101|98|25|20|100.0|TSTAT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads("[]")
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)

    # test_daybreak_alert tests what happens when an alert breaks the day boundary
    def test_daybreak_alert(self) -> None:
        mock_input = """
20180101 23:59:56.000|1000|101|98|25|20|100.0|TSTAT
20180101 23:59:57.000|1000|101|98|25|20|103.0|TSTAT
20180101 23:59:58.000|1000|101|98|25|20|102.0|TSTAT
20180101 23:59:59.000|1000|101|98|25|20|100.0|TSTAT
20180101 00:00:00.000|1000|101|98|25|20|100.0|TSTAT
20180101 00:00:01.000|1000|101|98|25|20|100.0|TSTAT
20180101 00:00:02.000|1000|101|98|25|20|102.0|TSTAT
20180101 00:00:03.000|1000|101|98|25|20|100.0|TSTAT
20180101 00:00:04.000|1000|101|98|25|20|100.0|TSTAT
        """.strip()
        with patch("builtins.open", mock_open(read_data=mock_input)):
            expected = json.loads(
                """
            [
                {
                    "satelliteId": 1000,
                    "severity": "RED HIGH",
                    "component": "TSTAT",
                    "timestamp": "2018-01-01T23:59:57.000Z"
                }
            ]
            """
            )
            with io.StringIO() as buf, redirect_stdout(
                buf
            ):  # redirecting stdout to a buffer instead
                main(
                    ["mockfile.dne"]
                )  # this file name is meaningless but the open call will be patched with the mock data as seen above
                output = buf.getvalue()
                actual = json.loads(output)
                self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
